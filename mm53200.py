"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pprint

class blk(gr.basic_block):  # other base classes are basic_block, decim_block, interp_block
	"""MM53200"""
	def __init__(self, samp_rate=32e3, code=[0,0,1,1,1,0,0,1,1,1,1,1], bit_time=750e-6, repeat=5):  # only default arguments here
		"""arguments to this function show up as parameters in GRC"""
		gr.basic_block.__init__(
			self,
			name='MM53200',   # will show up in GRC
			in_sig=None,
			out_sig=[np.byte]
		)
		# if an attribute with the same name as a parameter is found,
		# a callback is registered (properties work, too).
		self.offset=0
		self.repeat=repeat
		self.code=np.array(code,dtype=np.byte)
		self.symbol_zero=np.array([0,1,1],dtype=np.byte)
		self.symbol_one=np.array([0,0,1],dtype=np.byte)
		self.start_symbol=np.array([1],dtype=np.byte)
		self.stop_symbol=np.array([0]*len(self.code)*len(self.symbol_zero),dtype=np.byte)
		self.samp_rate=samp_rate
		self.bit_time=bit_time
		self.samples_per_bit=int(self.bit_time*self.samp_rate)
		if self.samples_per_bit<10:
			raise Exception(str(self.samples_per_bit)+' is not good (at least 10)')
		self.buffer=self.calc_output_bits()

	def calc_output_bits(self):
		out = np.repeat(self.start_symbol,self.samples_per_bit)
		for item in self.code:
			if item==0:
				out = np.append(out,np.repeat(self.symbol_zero,self.samples_per_bit))	
			else:
				out = np.append(out,np.repeat(self.symbol_one,self.samples_per_bit))
		out = np.append(out,np.repeat(self.stop_symbol,self.samples_per_bit))
		return out

	def general_work(self, input_items, output_items):
		c = 0
		if self.offset>=len(self.buffer) and self.repeat==0:
			return -1
		n = np.minimum((len(self.buffer)-self.offset)+(self.repeat*len(self.buffer)), len(output_items[0]))
		for i in xrange(n):
			output_items[0][i]=self.buffer[self.offset]
			self.offset += 1
			c += 1
			if self.offset>=len(self.buffer):
				if self.repeat>0:
					self.repeat-=1
					self.offset=0
				else:
					return c
		return c
