"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr


class nrzi_decoder(gr.sync_block):
    """
    docstring for block nrzi_encoder
    """
    def __init__(self):
        gr.sync_block.__init__(self,
            name="nrzi_decoder",
            in_sig=[np.uint8],
            out_sig=[np.uint8])

        self.last = 0

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        for i, x in enumerate(in0):
            if x==self.last:
                out[i]=1
            else:
                out[i]=0
            self.last = x

        return len(out)