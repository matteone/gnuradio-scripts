#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# Generated: Fri Dec 27 06:45:48 2019
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import epy_block_2
import math
import sys
from gnuradio import qtgui


class top_block(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Top Block")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Top Block")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.space = space = 1200
        self.mark = mark = 2200
        self.sym_dev = sym_dev = (mark-space)/2
        self.samp_rate = samp_rate = 48000
        self.points = points = 16384

        self.lowpass = lowpass = firdes.low_pass(1.0, samp_rate, sym_dev*2, sym_dev*0.8, firdes.WIN_HAMMING, 6.76)

        self.baud = baud = 1200

        ##################################################
        # Blocks
        ##################################################
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_fcc(1, (lowpass), (min(mark,space)+sym_dev), samp_rate)
        self.epy_block_2 = epy_block_2.blk()
        self.digital_hdlc_deframer_bp_0 = digital.hdlc_deframer_bp(10, 64)
        self.digital_diff_decoder_bb_0 = digital.diff_decoder_bb(2)
        self.digital_clock_recovery_mm_xx_0 = digital.clock_recovery_mm_ff(samp_rate/baud, samp_rate/baud*0.01, .5, 0.005, 0.005)
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_wavfile_source_0_0 = blocks.wavfile_source('/home/matteo/Documents/gnuradio/packet12gr.wav', False)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samp_rate,True)
        self.blocks_not_xx_0 = blocks.not_bb()
        self.blocks_and_const_xx_0 = blocks.and_const_bb(1)
        self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(20*baud/(2*math.pi*sym_dev))

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_hdlc_deframer_bp_0, 'out'), (self.epy_block_2, 'in'))
        self.connect((self.analog_quadrature_demod_cf_0, 0), (self.digital_clock_recovery_mm_xx_0, 0))
        self.connect((self.blocks_and_const_xx_0, 0), (self.digital_hdlc_deframer_bp_0, 0))
        self.connect((self.blocks_not_xx_0, 0), (self.blocks_and_const_xx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))
        self.connect((self.blocks_wavfile_source_0_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.digital_diff_decoder_bb_0, 0))
        self.connect((self.digital_clock_recovery_mm_xx_0, 0), (self.digital_binary_slicer_fb_0, 0))
        self.connect((self.digital_diff_decoder_bb_0, 0), (self.blocks_not_xx_0, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.analog_quadrature_demod_cf_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_space(self):
        return self.space

    def set_space(self, space):
        self.space = space
        self.set_sym_dev((self.mark-self.space)/2)
        self.freq_xlating_fir_filter_xxx_0.set_center_freq((min(self.mark,self.space)+self.sym_dev))

    def get_mark(self):
        return self.mark

    def set_mark(self, mark):
        self.mark = mark
        self.set_sym_dev((self.mark-self.space)/2)
        self.freq_xlating_fir_filter_xxx_0.set_center_freq((min(self.mark,self.space)+self.sym_dev))

    def get_sym_dev(self):
        return self.sym_dev

    def set_sym_dev(self, sym_dev):
        self.sym_dev = sym_dev
        self.freq_xlating_fir_filter_xxx_0.set_center_freq((min(self.mark,self.space)+self.sym_dev))
        self.analog_quadrature_demod_cf_0.set_gain(20*self.baud/(2*math.pi*self.sym_dev))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.digital_clock_recovery_mm_xx_0.set_omega(self.samp_rate/self.baud)
        self.digital_clock_recovery_mm_xx_0.set_gain_omega(self.samp_rate/self.baud*0.01)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_points(self):
        return self.points

    def set_points(self, points):
        self.points = points

    def get_lowpass(self):
        return self.lowpass

    def set_lowpass(self, lowpass):
        self.lowpass = lowpass
        self.freq_xlating_fir_filter_xxx_0.set_taps((self.lowpass))

    def get_baud(self):
        return self.baud

    def set_baud(self, baud):
        self.baud = baud
        self.digital_clock_recovery_mm_xx_0.set_omega(self.samp_rate/self.baud)
        self.digital_clock_recovery_mm_xx_0.set_gain_omega(self.samp_rate/self.baud*0.01)
        self.analog_quadrature_demod_cf_0.set_gain(20*self.baud/(2*math.pi*self.sym_dev))


def main(top_block_cls=top_block, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
