#!/usr/bin/env python

from gnuradio import gr,usrp,blks
from gnuradio.eng_option import eng_option
from optparse import OptionParser
from math import pi
from ax25 import *

#
# 1200 (128) 153600 (5/3) 256000 (500) 128e6
#

def main():
    parser=OptionParser(option_class=eng_option)
    parser.add_option("-f", "--freq", type="eng_float", default=144.800e6,help="set frequency to FREQ", metavar="FREQ")
    parser.add_option("-m", "--message", type="string", default=":ALL      :this is a test",help="message to send", metavar="MESSAGE")
    parser.add_option("-c", "--mycall", type="string", default="MYCALL", help="source callsign", metavar="CALL")
    parser.add_option("-t", "--tocall", type="string", default="CQ", help="recipient callsign", metavar="CALL")
    parser.add_option("-v", "--via", type="string", default="RELAY", help="digipeater callsign", metavar="CALL")
    parser.add_option("-d", "--do-logging", action="store_true", default=False, help="enable logging on datafiles")
    parser.add_option("-s", "--use-datafile", action="store_true", default=False, help="use usrp.dat (256kbps) as output")
    (options, args) = parser.parse_args()
    if len(args) !=0:
        parser.print_help()
        sys.exit(1)
    
    bitrate=1200
    markfreq=2200
    spacefreq=1200
    dac_rate=128e6
    usrp_interp=500
    cordic_freq=options.freq-dac_rate
    sf=153600
    syminterp=sf/bitrate
    symdev=abs(markfreq-spacefreq)/2
    cf=min(markfreq,spacefreq)+symdev
    symsens=2*pi*symdev/sf
    sw_interp=sf/bitrate
    nbfmdev=2e3    
    fmsens=2*pi*nbfmdev/(sf*5/3)
   
    #message=":ALL      :this is a test";
    #buildpacket("MYCALL",0,"CQ",0,"RELAY",0,0x03,0xf0,message)
    p=buildpacket(options.mycall,0,options.tocall,0,options.via,0,0x03,0xf0,options.message)
    if options.do_logging:
        dumppackettofile(p,"packet.dat")
    v=bits2syms(nrziencode(hdlcpacket(p,20,200)))
    
    fg = gr.flow_graph()
    symsrc = gr.vector_source_f(v)
    sqv = (1,) * syminterp
    symint = gr.interp_fir_filter_fff(syminterp, sqv)                   #1200*128=153600
    symmod = gr.frequency_modulator_fc(symsens)
    lo = gr.sig_source_c(sf, gr.GR_SIN_WAVE, cf, 1)
    mix = gr.multiply_cc()    
    c2r = gr.complex_to_real()
    res_taps = blks.design_filter(5,3,0.4)
    res = blks.rational_resampler_ccf(fg,5,3,res_taps)                  #153600*5/3=256000
    fmmod = gr.frequency_modulator_fc(fmsens)
    amp = gr.multiply_const_cc(32000)
    
    if options.use_datafile:
        dst=gr.file_sink(gr.sizeof_gr_complex,"usrp.dat")
    else:
        u = usrp.sink_c(0,usrp_interp)                                  #256000*500=128000000
        tx_subdev_spec = usrp.pick_tx_subdevice(u)
        m = usrp.determine_tx_mux_value(u, tx_subdev_spec)
        print "mux = %#04x" % (m,)
        u.set_mux(m)
        subdev = usrp.selected_subdev(u, tx_subdev_spec)
        print "Using TX d'board %s" % (subdev.side_and_name(),)
        u.set_tx_freq (0, cordic_freq)
        u.set_pga(0,0)
        print "Actual frequency: ",u.tx_freq(0)
        dst=u
       
    fg.connect(lo,(mix,0))
    fg.connect(symsrc,symint,symmod,(mix,1))
    fg.connect(mix,res,c2r,fmmod,amp,dst)
    
    fg.start()   
    fg.wait()
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass        
