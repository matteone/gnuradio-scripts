#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Hackrf Cw Tx
# GNU Radio version: 3.8.0.0

from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import epy_block_0
import osmosdr
import time

class hackrf_cw_tx(gr.top_block):

    def __init__(self, freq=2400100000, rfg=61, wpm=20):
        gr.top_block.__init__(self, "Hackrf Cw Tx")

        ##################################################
        # Parameters
        ##################################################
        self.freq = freq
        self.rfg = rfg
        self.wpm = wpm

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 1000000
        self.bb_rate = bb_rate = 10000
        self.rrc_taps = rrc_taps = 50
        self.lp = lp = firdes.low_pass(int(samp_rate/bb_rate), samp_rate, bb_rate/2,bb_rate/10, firdes.WIN_HAMMING, 6.76)

        ##################################################
        # Blocks
        ##################################################
        self.root_raised_cosine_filter_1 = filter.interp_fir_filter_ccf(
            1,
            firdes.root_raised_cosine(
                1,
                bb_rate,
                5,
                0.35,
                rrc_taps))
        self.root_raised_cosine_filter_0 = filter.fir_filter_ccf(
            1,
            firdes.root_raised_cosine(
                1,
                bb_rate,
                5,
                0.35,
                rrc_taps))
        self.osmosdr_source_0 = osmosdr.source(
            args="numchan=" + str(1) + " " + 'soapy=0,driver=hackrf'
        )
        self.osmosdr_source_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_source_0.set_sample_rate(samp_rate)
        self.osmosdr_source_0.set_center_freq(freq, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_gain(0, 0)
        self.osmosdr_source_0.set_if_gain(0, 0)
        self.osmosdr_source_0.set_bb_gain(0, 0)
        self.osmosdr_source_0.set_antenna('', 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)
        self.osmosdr_sink_0 = osmosdr.sink(
            args="numchan=" + str(1) + " " + 'soapy=0,driver=hackrf'
        )
        self.osmosdr_sink_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_sink_0.set_sample_rate(samp_rate)
        self.osmosdr_sink_0.set_center_freq(freq, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(rfg, 0)
        self.osmosdr_sink_0.set_if_gain(0, 0)
        self.osmosdr_sink_0.set_bb_gain(0, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_ccc(int(samp_rate/bb_rate), lp)
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)
        self.epy_block_0 = epy_block_0.blk()
        self.blocks_udp_source_0 = blocks.udp_source(gr.sizeof_char*1, '0.0.0.0', 1234, 1472, True)
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_gr_complex*1, int(1.2 * bb_rate / wpm))
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_repeat_0, 0), (self.root_raised_cosine_filter_1, 0))
        self.connect((self.blocks_udp_source_0, 0), (self.epy_block_0, 0))
        self.connect((self.epy_block_0, 0), (self.blocks_repeat_0, 0))
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.osmosdr_sink_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.root_raised_cosine_filter_0, 0), (self.interp_fir_filter_xxx_0, 0))
        self.connect((self.root_raised_cosine_filter_1, 0), (self.root_raised_cosine_filter_0, 0))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_sink_0.set_center_freq(self.freq, 0)
        self.osmosdr_source_0.set_center_freq(self.freq, 0)

    def get_rfg(self):
        return self.rfg

    def set_rfg(self, rfg):
        self.rfg = rfg
        self.osmosdr_sink_0.set_gain(self.rfg, 0)

    def get_wpm(self):
        return self.wpm

    def set_wpm(self, wpm):
        self.wpm = wpm
        self.blocks_repeat_0.set_interpolation(int(1.2 * self.bb_rate / self.wpm))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate)
        self.osmosdr_source_0.set_sample_rate(self.samp_rate)

    def get_bb_rate(self):
        return self.bb_rate

    def set_bb_rate(self, bb_rate):
        self.bb_rate = bb_rate
        self.blocks_repeat_0.set_interpolation(int(1.2 * self.bb_rate / self.wpm))
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1, self.bb_rate, 5, 0.35, self.rrc_taps))
        self.root_raised_cosine_filter_1.set_taps(firdes.root_raised_cosine(1, self.bb_rate, 5, 0.35, self.rrc_taps))

    def get_rrc_taps(self):
        return self.rrc_taps

    def set_rrc_taps(self, rrc_taps):
        self.rrc_taps = rrc_taps
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1, self.bb_rate, 5, 0.35, self.rrc_taps))
        self.root_raised_cosine_filter_1.set_taps(firdes.root_raised_cosine(1, self.bb_rate, 5, 0.35, self.rrc_taps))

    def get_lp(self):
        return self.lp

    def set_lp(self, lp):
        self.lp = lp
        self.interp_fir_filter_xxx_0.set_taps(self.lp)


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--freq", dest="freq", type=eng_float, default="2.4001G",
        help="Set freq [default=%(default)r]")
    parser.add_argument(
        "--rfg", dest="rfg", type=intx, default=61,
        help="Set RF Gain [default=%(default)r]")
    parser.add_argument(
        "-w", "--wpm", dest="wpm", type=intx, default=20,
        help="Set wpm [default=%(default)r]")
    return parser


def main(top_block_cls=hackrf_cw_tx, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(freq=options.freq, rfg=options.rfg, wpm=options.wpm)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
