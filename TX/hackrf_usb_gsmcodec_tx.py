#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Hackrf Usb Gsmcodec Tx
# GNU Radio version: 3.8.0.0

from gnuradio import analog
from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import vocoder
import osmosdr
import time

class hackrf_usb_gsmcodec_tx(gr.top_block):

    def __init__(self, freq=2400450000, gain=2, rfg=45):
        gr.top_block.__init__(self, "Hackrf Usb Gsmcodec Tx")

        ##################################################
        # Parameters
        ##################################################
        self.freq = freq
        self.gain = gain
        self.rfg = rfg

        ##################################################
        # Variables
        ##################################################
        self.rf_rate = rf_rate = 2000000
        self.if_rate = if_rate = 200000
        self.lp = lp = firdes.low_pass(int(rf_rate/if_rate), rf_rate, if_rate/2,if_rate/8, firdes.WIN_HAMMING, 6.76)
        self.test = test = len(lp)
        self.samp_rate = samp_rate = 8000
        self.audio_rate = audio_rate = 16000

        ##################################################
        # Blocks
        ##################################################
        self.vocoder_gsm_fr_decode_ps_0 = vocoder.gsm_fr_decode_ps()
        self.osmosdr_sink_0 = osmosdr.sink(
            args="numchan=" + str(1) + " " + 'soapy=0,driver=hackrf'
        )
        self.osmosdr_sink_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_sink_0.set_sample_rate(rf_rate)
        self.osmosdr_sink_0.set_center_freq(freq, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(rfg, 0)
        self.osmosdr_sink_0.set_if_gain(0, 0)
        self.osmosdr_sink_0.set_bb_gain(0, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_ccc(int(rf_rate/if_rate), lp)
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)
        self.blocks_udp_source_0 = blocks.udp_source(gr.sizeof_char*1, '0.0.0.0', 1234, 1472, True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_char*1, 33)
        self.blocks_short_to_float_0 = blocks.short_to_float(1, 8192)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(gain)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.band_pass_filter_0 = filter.interp_fir_filter_ccc(
            int(if_rate/samp_rate),
            firdes.complex_band_pass(
                int(if_rate/samp_rate),
                if_rate,
                200,
                2800,
                200,
                firdes.WIN_HAMMING,
                6.76))
        self.analog_const_source_x_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0_0, 0), (self.blocks_float_to_complex_0_0, 1))
        self.connect((self.band_pass_filter_0, 0), (self.interp_fir_filter_xxx_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.band_pass_filter_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.osmosdr_sink_0, 0))
        self.connect((self.blocks_short_to_float_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.vocoder_gsm_fr_decode_ps_0, 0))
        self.connect((self.blocks_udp_source_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.vocoder_gsm_fr_decode_ps_0, 0), (self.blocks_short_to_float_0, 0))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_sink_0.set_center_freq(self.freq, 0)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.blocks_multiply_const_vxx_0.set_k(self.gain)

    def get_rfg(self):
        return self.rfg

    def set_rfg(self, rfg):
        self.rfg = rfg
        self.osmosdr_sink_0.set_gain(self.rfg, 0)

    def get_rf_rate(self):
        return self.rf_rate

    def set_rf_rate(self, rf_rate):
        self.rf_rate = rf_rate
        self.osmosdr_sink_0.set_sample_rate(self.rf_rate)

    def get_if_rate(self):
        return self.if_rate

    def set_if_rate(self, if_rate):
        self.if_rate = if_rate
        self.band_pass_filter_0.set_taps(firdes.complex_band_pass(int(self.if_rate/self.samp_rate), self.if_rate, 200, 2800, 200, firdes.WIN_HAMMING, 6.76))

    def get_lp(self):
        return self.lp

    def set_lp(self, lp):
        self.lp = lp
        self.set_test(len(self.lp))
        self.interp_fir_filter_xxx_0.set_taps(self.lp)

    def get_test(self):
        return self.test

    def set_test(self, test):
        self.test = test

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.band_pass_filter_0.set_taps(firdes.complex_band_pass(int(self.if_rate/self.samp_rate), self.if_rate, 200, 2800, 200, firdes.WIN_HAMMING, 6.76))

    def get_audio_rate(self):
        return self.audio_rate

    def set_audio_rate(self, audio_rate):
        self.audio_rate = audio_rate


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--freq", dest="freq", type=eng_float, default="2.40045G",
        help="Set freq [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default="2.0",
        help="Set gain [default=%(default)r]")
    parser.add_argument(
        "--rfg", dest="rfg", type=intx, default=45,
        help="Set RF Gain [default=%(default)r]")
    return parser


def main(top_block_cls=hackrf_usb_gsmcodec_tx, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(freq=options.freq, gain=options.gain, rfg=options.rfg)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
