options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: top_block
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: ''
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [9, 8]
    rotation: 0
    state: enabled

blocks:
- name: bb_rate
  id: variable
  parameters:
    comment: ''
    value: psk31_baud*samples_per_symbol
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [514, 13]
    rotation: 0
    state: enabled
- name: c
  id: variable_constellation
  parameters:
    comment: ''
    const_points: '[-1,-0.7071+0.7071j,1j,0.7071+0.7071j,1,0.7071-0.7071j,-1j,-0.7071-0.7071j]'
    dims: '1'
    precision: '8'
    rot_sym: '8'
    soft_dec_lut: None
    sym_map: '[4,5,6,7,0,1,2,3]'
    type: calcdist
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [841, 142]
    rotation: 0
    state: enabled
- name: gain
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: gain
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '0'
    step: '.01'
    stop: '1'
    value: '.1'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [923, 384]
    rotation: 0
    state: disabled
- name: if_rate
  id: variable
  parameters:
    comment: ''
    value: '100000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [594, 13]
    rotation: 0
    state: enabled
- name: lp
  id: variable_low_pass_filter_taps
  parameters:
    beta: '6.76'
    comment: ''
    cutoff_freq: if_rate/2
    gain: int(samp_rate/if_rate)
    samp_rate: samp_rate
    width: if_rate/tr_factor
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [721, 380]
    rotation: 0
    state: enabled
- name: lp_0
  id: variable_low_pass_filter_taps
  parameters:
    beta: '6.76'
    comment: ''
    cutoff_freq: bb_rate/2
    gain: int(if_rate/bb_rate)
    samp_rate: if_rate
    width: bb_rate/tr_factor
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 376]
    rotation: 0
    state: enabled
- name: psk31_baud
  id: variable
  parameters:
    comment: ''
    value: '500'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [274, 13]
    rotation: 0
    state: enabled
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: '1000000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [183, 12]
    rotation: 0
    state: enabled
- name: samples_per_symbol
  id: variable
  parameters:
    comment: ''
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [370, 13]
    rotation: 0
    state: enabled
- name: test
  id: variable
  parameters:
    comment: ''
    value: len(lp)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 524]
    rotation: 0
    state: enabled
- name: test_0
  id: variable
  parameters:
    comment: ''
    value: len(lp_0)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 516]
    rotation: 0
    state: enabled
- name: tr_factor
  id: variable
  parameters:
    comment: ''
    value: '5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [606, 521]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: '1500'
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: '48000'
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [426, 641]
    rotation: 0
    state: disabled
- name: audio_sink_0
  id: audio_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    device_name: ''
    num_inputs: '1'
    ok_to_block: 'True'
    samp_rate: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1126, 623]
    rotation: 0
    state: disabled
- name: bbg
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: baseband gain
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [982, 12]
    rotation: 0
    state: true
- name: blocks_complex_to_float_0
  id: blocks_complex_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [950, 627]
    rotation: 0
    state: disabled
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: gain
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [937, 325]
    rotation: 0
    state: enabled
- name: blocks_multiply_const_vxx_0_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: '.15'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [790, 639]
    rotation: 0
    state: disabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [654, 627]
    rotation: 0
    state: disabled
- name: blocks_null_sink_0
  id: blocks_null_sink
  parameters:
    affinity: ''
    alias: ''
    bus_structure_sink: '[[0,],]'
    comment: ''
    num_inputs: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [236, 549]
    rotation: 0
    state: enabled
- name: blocks_repack_bits_bb_0
  id: blocks_repack_bits_bb
  parameters:
    affinity: ''
    alias: ''
    align_output: 'False'
    comment: ''
    endianness: gr.GR_LSB_FIRST
    k: '1'
    l: '3'
    len_tag_key: '""'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [303, 148]
    rotation: 0
    state: enabled
- name: blocks_udp_source_0
  id: blocks_udp_source
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    eof: 'True'
    ipaddr: 0.0.0.0
    maxoutbuf: '0'
    minoutbuf: '0'
    port: '1234'
    psize: '1472'
    type: byte
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [10, 161]
    rotation: 0
    state: disabled
- name: blocks_unpacked_to_packed_xx_0
  id: blocks_unpacked_to_packed_xx
  parameters:
    affinity: ''
    alias: ''
    bits_per_chunk: '3'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [469, 81]
    rotation: 0
    state: enabled
- name: blocks_vector_source_x_2_1_0
  id: blocks_vector_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    repeat: 'True'
    tags: '[]'
    type: byte
    vector: '[ ord(i) for i in "This is IZ2EEQ testing\n"]'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 84]
    rotation: 0
    state: enabled
- name: digital_constellation_modulator_0
  id: digital_constellation_modulator
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: c
    differential: 'True'
    excess_bw: '.35'
    log: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_symbol: samples_per_symbol
    verbose: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [609, 153]
    rotation: 0
    state: enabled
- name: eight_psk_encoder_0
  id: epy_block
  parameters:
    _source_code: "\"\"\"\nEmbedded Python Blocks:\n\nEach time this file is saved,\
      \ GRC will instantiate the first class it finds\nto get ports and parameters\
      \ of your block. The arguments to __init__  will\nbe the parameters. All of\
      \ them are required to have default values!\n\"\"\"\n\nimport numpy as np\n\
      from gnuradio import gr\nimport pmt\n\nclass blk(gr.basic_block):  # other base\
      \ classes are basic_block, decim_block, interp_block\n\n\tdef __init__(self):\
      \  # only default arguments here\n\t\t\"\"\"arguments to this function show\
      \ up as parameters in GRC\"\"\"\n\t\tgr.basic_block.__init__(\n\t\t\tself,\n\
      \t\t\tname='8PSK Encoder',   # will show up in GRC\n\t\t\tin_sig=[np.byte],\n\
      \t\t\tout_sig=[np.byte]\n\t\t)\n\t\tself.eol = True\n\t\tself.varicode = [\n\
      \t\t\t\"11101011100\",\n\t\t\t\"11101100000\",\n\t\t\t\"11101101000\",\n\t\t\
      \t\"11101101100\",\t\t\n\t\t\t\"11101110000\",\t\t\n\t\t\t\"11101110100\",\t\
      \t\n\t\t\t\"11101111000\",\t\t\n\t\t\t\"11101111100\",\t\t\n\t\t\t\"10101000\"\
      ,\t\t\t\n\t\t\t\"11110000000\",\t\t\n\t\t\t\"11110100000\",\t\t\n\t\t\t\"11110101000\"\
      ,\t\t\n\t\t\t\"11110101100\",\t\t\n\t\t\t\"10101100\",\t\t\t\n\t\t\t\"11110110000\"\
      ,\t\t\n\t\t\t\"11110110100\",\t\t\n\t\t\t\"11110111000\",\t\t\n\t\t\t\"11110111100\"\
      ,\t\t\n\t\t\t\"11111000000\",\t\t\n\t\t\t\"11111010000\",\t\t\n\t\t\t\"11111010100\"\
      ,\t\t\n\t\t\t\"11111011000\",\t\t\n\t\t\t\"11111011100\",\t\t\n\t\t\t\"11111100000\"\
      ,\t\t\n\t\t\t\"11111101000\",\t\t\n\t\t\t\"11111101100\",\t\t\n\t\t\t\"11111110000\"\
      ,\t\t\n\t\t\t\"11111110100\",\t\t\n\t\t\t\"11111111000\",\t\t\n\t\t\t\"11111111100\"\
      ,\t\t\n\t\t\t\"100000000000\",\t\t\n\t\t\t\"101000000000\",\t\t\n\t\t\t\"100\"\
      ,\t\t\t\t\n\t\t\t\"111000000\",\t\n\t\t\t\"111111100\",\t\n\t\t\t\"1011011000\"\
      ,\t\n\t\t\t\"1010101000\",\t\n\t\t\t\"1010100000\",\t\n\t\t\t\"1000000000\"\
      ,\t\n\t\t\t\"110111100\",\t\n\t\t\t\"111110100\",\t\n\t\t\t\"111110000\",\t\n\
      \t\t\t\"1010110100\",\t\n\t\t\t\"111100000\",\t\n\t\t\t\"10100000\",\t\t\n\t\
      \t\t\"111011000\",\t\n\t\t\t\"111010100\",\t\n\t\t\t\"111101000\",\t\n\t\t\t\
      \"11100000\",\t\t\n\t\t\t\"11110000\",\t\t\n\t\t\t\"101000000\",\t\n\t\t\t\"\
      101010100\",\t\n\t\t\t\"101110100\",\t\n\t\t\t\"101100000\",\t\n\t\t\t\"101101100\"\
      ,\t\n\t\t\t\"110100000\",\t\n\t\t\t\"110000000\",\t\n\t\t\t\"110101100\",\t\n\
      \t\t\t\"111101100\",\t\n\t\t\t\"111111000\",\t\n\t\t\t\"1011000000\",\t\n\t\t\
      \t\"111011100\",\t\n\t\t\t\"1010111100\",\t\n\t\t\t\"111010000\",\t\n\t\t\t\"\
      1010000000\",\t\n\t\t\t\"10111100\",\t\t\n\t\t\t\"100000000\",\t\n\t\t\t\"11010100\"\
      ,\t\t\n\t\t\t\"11011100\",\t\t\n\t\t\t\"10111000\",\t\t\n\t\t\t\"11111000\"\
      ,\t\t\n\t\t\t\"101010000\",\t\n\t\t\t\"101011000\",\t\n\t\t\t\"11000000\",\t\
      \t\n\t\t\t\"110110100\",\t\n\t\t\t\"101111100\",\t\n\t\t\t\"11110100\",\t\t\n\
      \t\t\t\"11101000\",\t\t\n\t\t\t\"11111100\",\t\t\n\t\t\t\"11010000\",\t\t\n\t\
      \t\t\"11101100\",\t\t\n\t\t\t\"110110000\",\t\n\t\t\t\"11011000\",\t\t\n\t\t\
      \t\"10110100\",\t\t\n\t\t\t\"10110000\",\t\t\n\t\t\t\"101011100\",\t\n\t\t\t\
      \"110101000\",\t\n\t\t\t\"101101000\",\t\n\t\t\t\"101110000\",\t\n\t\t\t\"101111000\"\
      ,\t\n\t\t\t\"110111000\",\t\n\t\t\t\"1011101000\",\t\n\t\t\t\"1011010000\",\t\
      \n\t\t\t\"1011101100\",\t\n\t\t\t\"1011010100\",\t\n\t\t\t\"1010110000\",\t\n\
      \t\t\t\"1010101100\",\t\n\t\t\t\"10100\",\t\t\n\t\t\t\"1100000\",\t\t\n\t\t\t\
      \"111000\",\t\t\n\t\t\t\"110100\",\t\t\n\t\t\t\"1000\",\t\t\t\n\t\t\t\"1010000\"\
      ,\t\t\n\t\t\t\"1011000\",\t\t\n\t\t\t\"110000\",\t\t\n\t\t\t\"11000\",\t\t\n\
      \t\t\t\"10000000\",\t\t\n\t\t\t\"1110000\",\t\t\n\t\t\t\"101100\",\t\t\n\t\t\
      \t\"1000000\",\t\t\n\t\t\t\"11100\",\t\t\n\t\t\t\"10000\",\t\t\n\t\t\t\"1010100\"\
      ,\t\t\n\t\t\t\"1111000\",\t\t\n\t\t\t\"100000\",\t\t\n\t\t\t\"101000\",\t\t\n\
      \t\t\t\"1100\",\t\t\t\n\t\t\t\"111100\",\t\t\n\t\t\t\"1101100\",\t\t\n\t\t\t\
      \"1101000\",\t\t\n\t\t\t\"1110100\",\t\t\n\t\t\t\"1011100\",\t\t\n\t\t\t\"1111100\"\
      ,\t\t\n\t\t\t\"1011011100\",\t\n\t\t\t\"1010111000\",\t\n\t\t\t\"1011100000\"\
      ,\t\n\t\t\t\"1011110000\",\t\n\t\t\t\"101010000000\",\t\t\n\t\t\t\"101010100000\"\
      ,\t\n\t\t\t\"101010101000\",\t\n\t\t\t\"101010101100\",\t\n\t\t\t\"101010110000\"\
      ,\t\n\t\t\t\"101010110100\",\t\n\t\t\t\"101010111000\",\t\n\t\t\t\"101010111100\"\
      ,\t\n\t\t\t\"101011000000\",\t\n\t\t\t\"101011010000\",\t\n\t\t\t\"101011010100\"\
      ,\t\n\t\t\t\"101011011000\",\t\n\t\t\t\"101011011100\",\t\n\t\t\t\"101011100000\"\
      ,\t\n\t\t\t\"101011101000\",\t\n\t\t\t\"101011101100\",\t\n\t\t\t\"101011110000\"\
      ,\t\n\t\t\t\"101011110100\",\t\n\t\t\t\"101011111000\",\t\n\t\t\t\"101011111100\"\
      ,\t\n\t\t\t\"101100000000\",\t\n\t\t\t\"101101000000\",\t\n\t\t\t\"101101010000\"\
      ,\t\n\t\t\t\"101101010100\",\t\n\t\t\t\"101101011000\",\t\n\t\t\t\"101101011100\"\
      ,\t\n\t\t\t\"101101100000\",\t\n\t\t\t\"101101101000\",\t\n\t\t\t\"101101101100\"\
      ,\t\n\t\t\t\"101101110000\",\t\n\t\t\t\"101101110100\",\t\n\t\t\t\"101101111000\"\
      ,\t\n\t\t\t\"101101111100\",\t\n\t\t\t\"1011110100\",\t\n\t\t\t\"1011111000\"\
      ,\t\n\t\t\t\"1011111100\",\t\n\t\t\t\"1100000000\",\t\n\t\t\t\"1101000000\"\
      ,\t\n\t\t\t\"1101010000\",\t\n\t\t\t\"1101010100\",\t\n\t\t\t\"1101011000\"\
      ,\t\n\t\t\t\"1101011100\",\t\n\t\t\t\"1101100000\",\t\n\t\t\t\"1101101000\"\
      ,\t\n\t\t\t\"1101101100\",\t\n\t\t\t\"1101110000\",\t\n\t\t\t\"1101110100\"\
      ,\t\n\t\t\t\"1101111000\",\t\n\t\t\t\"1101111100\",\t\n\t\t\t\"1110000000\"\
      ,\t\n\t\t\t\"1110100000\",\t\n\t\t\t\"1110101000\",\t\n\t\t\t\"1110101100\"\
      ,\t\n\t\t\t\"1110110000\",\t\n\t\t\t\"1110110100\",\t\n\t\t\t\"1110111000\"\
      ,\t\n\t\t\t\"1110111100\",\t\n\t\t\t\"1111000000\",\t\n\t\t\t\"1111010000\"\
      ,\t\n\t\t\t\"1111010100\",\t\n\t\t\t\"1111011000\",\t\n\t\t\t\"1111011100\"\
      ,\t\n\t\t\t\"1111100000\",\t\n\t\t\t\"1111101000\",\t\n\t\t\t\"1111101100\"\
      ,\t\n\t\t\t\"1111110000\",\t\n\t\t\t\"1111110100\",\t\n\t\t\t\"1111111000\"\
      ,\t\n\t\t\t\"1111111100\",\t\n\t\t\t\"10000000000\",\t\n\t\t\t\"10100000000\"\
      ,\t\n\t\t\t\"10101000000\",\t\n\t\t\t\"10101010000\",\t\n\t\t\t\"10101010100\"\
      ,\t\n\t\t\t\"10101011000\",\t\n\t\t\t\"10101011100\",\t\n\t\t\t\"10101100000\"\
      ,\t\n\t\t\t\"10101101000\",\t\n\t\t\t\"10101101100\",\t\n\t\t\t\"10101110000\"\
      ,\t\n\t\t\t\"10101110100\",\t\n\t\t\t\"10101111000\",\t\n\t\t\t\"10101111100\"\
      ,\t\n\t\t\t\"10110000000\",\t\n\t\t\t\"10110100000\",\t\n\t\t\t\"10110101000\"\
      ,\t\n\t\t\t\"10110101100\",\t\n\t\t\t\"10110110000\",\t\n\t\t\t\"10110110100\"\
      ,\t\n\t\t\t\"10110111000\",\t\n\t\t\t\"10110111100\",\t\n\t\t\t\"10111000000\"\
      ,\t\n\t\t\t\"10111010000\",\t\n\t\t\t\"10111010100\",\t\n\t\t\t\"10111011000\"\
      ,\t\n\t\t\t\"10111011100\",\t\n\t\t\t\"10111100000\",\t\n\t\t\t\"10111101000\"\
      ,\t\n\t\t\t\"10111101100\",\t\n\t\t\t\"10111110000\",\t\n\t\t\t\"10111110100\"\
      ,\t\n\t\t\t\"10111111000\",\t\n\t\t\t\"10111111100\",\t\n\t\t\t\"11000000000\"\
      ,\t\n\t\t\t\"11010000000\",\t\n\t\t\t\"11010100000\",\t\n\t\t\t\"11010101000\"\
      ,\t\n\t\t\t\"11010101100\",\t\n\t\t\t\"11010110000\",\t\n\t\t\t\"11010110100\"\
      ,\t\n\t\t\t\"11010111000\",\t\n\t\t\t\"11010111100\",\t\n\t\t\t\"11011000000\"\
      ,\t\n\t\t\t\"11011010000\",\t\n\t\t\t\"11011010100\",\t\n\t\t\t\"11011011000\"\
      ,\t\n\t\t\t\"11011011100\",\t\n\t\t\t\"11011100000\",\t\n\t\t\t\"11011101000\"\
      ,\t\n\t\t\t\"11011101100\",\t\n\t\t\t\"11011110000\",\t\n\t\t\t\"11011110100\"\
      ,\t\n\t\t\t\"11011111000\",\t\n\t\t\t\"11011111100\",\t\n\t\t\t\"11100000000\"\
      ,\t\n\t\t\t\"11101000000\",\t\n\t\t\t\"11101010000\",\t\n\t\t\t\"11101010100\"\
      ,\t\n\t\t\t\"11101011000\"\n\t\t]\n\n\tdef general_work(self, input_items, output_items):\n\
      \t\t#print \"requested output items:\", len(output_items[0])\n\t\t#print \"\
      passed input items\", len(input_items[0])\n\t\tif len(input_items[0]) == 0:\n\
      \t\t\t#print \"no input items\"\n\t\t\treturn 0\n\t\toutput = self.translate_text([input_items[0][0]])\n\
      \t\t#print \"produced items:\", len(output)\n\t\tif len(output) > len(output_items[0]):\n\
      \t\t\t#print \"too few output items requested\"\n\t\t\treturn 0\n\t\toutput_items[0][:len(output)]\
      \ = [ x for x in output ]\n\t\tself.consume_each(1)\n\t\treturn len(output)\n\
      \n\tdef forecast(self, ninput_items_required, noutput_items):\n\t\t#print noutput_items\n\
      \t\tninput_items_required = int(noutput_items / 14)\n\t\treturn ninput_items_required\n\
      \n\tdef translate_text(self,bytearray):\n\t\ttranslation = []\n\t\tfor byte\
      \ in bytearray:\n\t\t\tcharcode = byte & 255\n\t\t\tvaricode = self.varicode[charcode]\n\
      \t\t\tif varicode:\n\t\t\t\tif self.eol:\n\t\t\t\t\t#key = pmt.intern(\"\")\n\
      \t\t\t\t\t#value = pmt.intern(\"tx_sob\")\n\t\t\t\t\t#self.add_item_tag(0, self.nitems_written(0),\
      \ key, value)\n\t\t\t\t\tself.eol = False\n\t\t\t\t\ttranslation.extend(self.varicode[0])\n\
      \t\t\t\t\ttranslation.extend([0] * 30)                \n\t\t\t\ttranslation.extend(map(int,list(varicode)))\n\
      \t\t\tif (charcode == 10):\n\t\t\t\ttranslation.extend(self.varicode[0])\n\t\
      \t\t\ttranslation.extend([0] * 30) \n\t\t\t\t#key = pmt.intern(\"\")\n\t\t\t\
      \t#value = pmt.intern(\"tx_eob\")\n\t\t\t\t#self.add_item_tag(0, self.nitems_written(0)+128,\
      \ key, value)   \n\t\t\t\tself.eol = True                \n\t\t\tprint(chr(charcode),charcode,varicode,translation)\n\
      \t\treturn translation\n\t\t\n"
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    _io_cache: ('8PSK Encoder', 'blk', [], [('0', 'byte', 1)], [('0', 'byte', 1)],
      'arguments to this function show up as parameters in GRC', [])
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [229, 88]
    rotation: 0
    state: enabled
- name: epy_block_1_0_1
  id: epy_block
  parameters:
    _source_code: "\"\"\"\nEmbedded Python Blocks:\n\nEach time this file is saved,\
      \ GRC will instantiate the first class it finds\nto get ports and parameters\
      \ of your block. The arguments to __init__  will\nbe the parameters. All of\
      \ them are required to have default values!\n\"\"\"\nimport sys\nimport numpy\
      \ as np\nfrom gnuradio import gr\n\n\nclass blk(gr.sync_block):  # other base\
      \ classes are basic_block, decim_block, interp_block\n    \"\"\"Embedded Python\
      \ Block example - a simple multiply const\"\"\"\n\n    def __init__(self): \
      \ # only default arguments here\n        \"\"\"arguments to this function show\
      \ up as parameters in GRC\"\"\"\n        gr.sync_block.__init__(\n         \
      \   self,\n            name='Byte Logger',   # will show up in GRC\n       \
      \     in_sig= [np.uint8],\n            out_sig= None\n        )\n        # if\
      \ an attribute with the same name as a parameter is found,\n        # a callback\
      \ is registered (properties work, too).\n\n    def work(self, input_items, output_items):\n\
      \        for item in input_items[0]:\n            sys.stdout.write(str(item)+'#')\n\
      \            sys.stdout.flush\n        print\n        return len(input_items[0])\n"
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    _io_cache: ('Byte Logger', 'blk', [], [('0', 'byte', 1)], [], 'Embedded Python
      Block example - a simple multiply const', [])
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [309, 224]
    rotation: 0
    state: disabled
- name: freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: freq
    short_id: f
    type: eng_float
    value: '2400000000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [704, 18]
    rotation: 0
    state: true
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: gain
    short_id: g
    type: eng_float
    value: '.15'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1109, 10]
    rotation: 0
    state: true
- name: ifg
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: IF gain
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [890, 14]
    rotation: 0
    state: true
- name: interp_fir_filter_xxx_0
  id: interp_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: int(samp_rate/if_rate)
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_delay: '0'
    taps: lp
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 315]
    rotation: 0
    state: enabled
- name: interp_fir_filter_xxx_0_0
  id: interp_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: int(if_rate/bb_rate)
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_delay: '0'
    taps: lp_0
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 316]
    rotation: 0
    state: enabled
- name: osmosdr_sink_0
  id: osmosdr_sink
  parameters:
    affinity: ''
    alias: ''
    ant0: ''
    ant1: ''
    ant10: ''
    ant11: ''
    ant12: ''
    ant13: ''
    ant14: ''
    ant15: ''
    ant16: ''
    ant17: ''
    ant18: ''
    ant19: ''
    ant2: ''
    ant20: ''
    ant21: ''
    ant22: ''
    ant23: ''
    ant24: ''
    ant25: ''
    ant26: ''
    ant27: ''
    ant28: ''
    ant29: ''
    ant3: ''
    ant30: ''
    ant31: ''
    ant4: ''
    ant5: ''
    ant6: ''
    ant7: ''
    ant8: ''
    ant9: ''
    args: soapy=0,driver=hackrf
    bb_gain0: '0'
    bb_gain1: '20'
    bb_gain10: '20'
    bb_gain11: '20'
    bb_gain12: '20'
    bb_gain13: '20'
    bb_gain14: '20'
    bb_gain15: '20'
    bb_gain16: '20'
    bb_gain17: '20'
    bb_gain18: '20'
    bb_gain19: '20'
    bb_gain2: '20'
    bb_gain20: '20'
    bb_gain21: '20'
    bb_gain22: '20'
    bb_gain23: '20'
    bb_gain24: '20'
    bb_gain25: '20'
    bb_gain26: '20'
    bb_gain27: '20'
    bb_gain28: '20'
    bb_gain29: '20'
    bb_gain3: '20'
    bb_gain30: '20'
    bb_gain31: '20'
    bb_gain4: '20'
    bb_gain5: '20'
    bb_gain6: '20'
    bb_gain7: '20'
    bb_gain8: '20'
    bb_gain9: '20'
    bw0: '0'
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    clock_source0: ''
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    corr0: '0'
    corr1: '0'
    corr10: '0'
    corr11: '0'
    corr12: '0'
    corr13: '0'
    corr14: '0'
    corr15: '0'
    corr16: '0'
    corr17: '0'
    corr18: '0'
    corr19: '0'
    corr2: '0'
    corr20: '0'
    corr21: '0'
    corr22: '0'
    corr23: '0'
    corr24: '0'
    corr25: '0'
    corr26: '0'
    corr27: '0'
    corr28: '0'
    corr29: '0'
    corr3: '0'
    corr30: '0'
    corr31: '0'
    corr4: '0'
    corr5: '0'
    corr6: '0'
    corr7: '0'
    corr8: '0'
    corr9: '0'
    freq0: freq
    freq1: 100e6
    freq10: 100e6
    freq11: 100e6
    freq12: 100e6
    freq13: 100e6
    freq14: 100e6
    freq15: 100e6
    freq16: 100e6
    freq17: 100e6
    freq18: 100e6
    freq19: 100e6
    freq2: 100e6
    freq20: 100e6
    freq21: 100e6
    freq22: 100e6
    freq23: 100e6
    freq24: 100e6
    freq25: 100e6
    freq26: 100e6
    freq27: 100e6
    freq28: 100e6
    freq29: 100e6
    freq3: 100e6
    freq30: 100e6
    freq31: 100e6
    freq4: 100e6
    freq5: 100e6
    freq6: 100e6
    freq7: 100e6
    freq8: 100e6
    freq9: 100e6
    gain0: '0'
    gain1: '10'
    gain10: '10'
    gain11: '10'
    gain12: '10'
    gain13: '10'
    gain14: '10'
    gain15: '10'
    gain16: '10'
    gain17: '10'
    gain18: '10'
    gain19: '10'
    gain2: '10'
    gain20: '10'
    gain21: '10'
    gain22: '10'
    gain23: '10'
    gain24: '10'
    gain25: '10'
    gain26: '10'
    gain27: '10'
    gain28: '10'
    gain29: '10'
    gain3: '10'
    gain30: '10'
    gain31: '10'
    gain4: '10'
    gain5: '10'
    gain6: '10'
    gain7: '10'
    gain8: '10'
    gain9: '10'
    if_gain0: '20'
    if_gain1: '20'
    if_gain10: '20'
    if_gain11: '20'
    if_gain12: '20'
    if_gain13: '20'
    if_gain14: '20'
    if_gain15: '20'
    if_gain16: '20'
    if_gain17: '20'
    if_gain18: '20'
    if_gain19: '20'
    if_gain2: '20'
    if_gain20: '20'
    if_gain21: '20'
    if_gain22: '20'
    if_gain23: '20'
    if_gain24: '20'
    if_gain25: '20'
    if_gain26: '20'
    if_gain27: '20'
    if_gain28: '20'
    if_gain29: '20'
    if_gain3: '20'
    if_gain30: '20'
    if_gain31: '20'
    if_gain4: '20'
    if_gain5: '20'
    if_gain6: '20'
    if_gain7: '20'
    if_gain8: '20'
    if_gain9: '20'
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    num_mboards: '1'
    sample_rate: samp_rate
    sync: sync
    time_source0: ''
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1148, 225]
    rotation: 0
    state: enabled
- name: osmosdr_source_0
  id: osmosdr_source
  parameters:
    affinity: ''
    alias: ''
    ant0: ''
    ant1: ''
    ant10: ''
    ant11: ''
    ant12: ''
    ant13: ''
    ant14: ''
    ant15: ''
    ant16: ''
    ant17: ''
    ant18: ''
    ant19: ''
    ant2: ''
    ant20: ''
    ant21: ''
    ant22: ''
    ant23: ''
    ant24: ''
    ant25: ''
    ant26: ''
    ant27: ''
    ant28: ''
    ant29: ''
    ant3: ''
    ant30: ''
    ant31: ''
    ant4: ''
    ant5: ''
    ant6: ''
    ant7: ''
    ant8: ''
    ant9: ''
    args: soapy=0,driver=hackrf
    bb_gain0: '0'
    bb_gain1: '20'
    bb_gain10: '20'
    bb_gain11: '20'
    bb_gain12: '20'
    bb_gain13: '20'
    bb_gain14: '20'
    bb_gain15: '20'
    bb_gain16: '20'
    bb_gain17: '20'
    bb_gain18: '20'
    bb_gain19: '20'
    bb_gain2: '20'
    bb_gain20: '20'
    bb_gain21: '20'
    bb_gain22: '20'
    bb_gain23: '20'
    bb_gain24: '20'
    bb_gain25: '20'
    bb_gain26: '20'
    bb_gain27: '20'
    bb_gain28: '20'
    bb_gain29: '20'
    bb_gain3: '20'
    bb_gain30: '20'
    bb_gain31: '20'
    bb_gain4: '20'
    bb_gain5: '20'
    bb_gain6: '20'
    bb_gain7: '20'
    bb_gain8: '20'
    bb_gain9: '20'
    bw0: '0'
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    clock_source0: ''
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    corr0: '0'
    corr1: '0'
    corr10: '0'
    corr11: '0'
    corr12: '0'
    corr13: '0'
    corr14: '0'
    corr15: '0'
    corr16: '0'
    corr17: '0'
    corr18: '0'
    corr19: '0'
    corr2: '0'
    corr20: '0'
    corr21: '0'
    corr22: '0'
    corr23: '0'
    corr24: '0'
    corr25: '0'
    corr26: '0'
    corr27: '0'
    corr28: '0'
    corr29: '0'
    corr3: '0'
    corr30: '0'
    corr31: '0'
    corr4: '0'
    corr5: '0'
    corr6: '0'
    corr7: '0'
    corr8: '0'
    corr9: '0'
    dc_offset_mode0: '0'
    dc_offset_mode1: '0'
    dc_offset_mode10: '0'
    dc_offset_mode11: '0'
    dc_offset_mode12: '0'
    dc_offset_mode13: '0'
    dc_offset_mode14: '0'
    dc_offset_mode15: '0'
    dc_offset_mode16: '0'
    dc_offset_mode17: '0'
    dc_offset_mode18: '0'
    dc_offset_mode19: '0'
    dc_offset_mode2: '0'
    dc_offset_mode20: '0'
    dc_offset_mode21: '0'
    dc_offset_mode22: '0'
    dc_offset_mode23: '0'
    dc_offset_mode24: '0'
    dc_offset_mode25: '0'
    dc_offset_mode26: '0'
    dc_offset_mode27: '0'
    dc_offset_mode28: '0'
    dc_offset_mode29: '0'
    dc_offset_mode3: '0'
    dc_offset_mode30: '0'
    dc_offset_mode31: '0'
    dc_offset_mode4: '0'
    dc_offset_mode5: '0'
    dc_offset_mode6: '0'
    dc_offset_mode7: '0'
    dc_offset_mode8: '0'
    dc_offset_mode9: '0'
    freq0: freq
    freq1: 100e6
    freq10: 100e6
    freq11: 100e6
    freq12: 100e6
    freq13: 100e6
    freq14: 100e6
    freq15: 100e6
    freq16: 100e6
    freq17: 100e6
    freq18: 100e6
    freq19: 100e6
    freq2: 100e6
    freq20: 100e6
    freq21: 100e6
    freq22: 100e6
    freq23: 100e6
    freq24: 100e6
    freq25: 100e6
    freq26: 100e6
    freq27: 100e6
    freq28: 100e6
    freq29: 100e6
    freq3: 100e6
    freq30: 100e6
    freq31: 100e6
    freq4: 100e6
    freq5: 100e6
    freq6: 100e6
    freq7: 100e6
    freq8: 100e6
    freq9: 100e6
    gain0: '0'
    gain1: '10'
    gain10: '10'
    gain11: '10'
    gain12: '10'
    gain13: '10'
    gain14: '10'
    gain15: '10'
    gain16: '10'
    gain17: '10'
    gain18: '10'
    gain19: '10'
    gain2: '10'
    gain20: '10'
    gain21: '10'
    gain22: '10'
    gain23: '10'
    gain24: '10'
    gain25: '10'
    gain26: '10'
    gain27: '10'
    gain28: '10'
    gain29: '10'
    gain3: '10'
    gain30: '10'
    gain31: '10'
    gain4: '10'
    gain5: '10'
    gain6: '10'
    gain7: '10'
    gain8: '10'
    gain9: '10'
    gain_mode0: 'False'
    gain_mode1: 'False'
    gain_mode10: 'False'
    gain_mode11: 'False'
    gain_mode12: 'False'
    gain_mode13: 'False'
    gain_mode14: 'False'
    gain_mode15: 'False'
    gain_mode16: 'False'
    gain_mode17: 'False'
    gain_mode18: 'False'
    gain_mode19: 'False'
    gain_mode2: 'False'
    gain_mode20: 'False'
    gain_mode21: 'False'
    gain_mode22: 'False'
    gain_mode23: 'False'
    gain_mode24: 'False'
    gain_mode25: 'False'
    gain_mode26: 'False'
    gain_mode27: 'False'
    gain_mode28: 'False'
    gain_mode29: 'False'
    gain_mode3: 'False'
    gain_mode30: 'False'
    gain_mode31: 'False'
    gain_mode4: 'False'
    gain_mode5: 'False'
    gain_mode6: 'False'
    gain_mode7: 'False'
    gain_mode8: 'False'
    gain_mode9: 'False'
    if_gain0: '0'
    if_gain1: '20'
    if_gain10: '20'
    if_gain11: '20'
    if_gain12: '20'
    if_gain13: '20'
    if_gain14: '20'
    if_gain15: '20'
    if_gain16: '20'
    if_gain17: '20'
    if_gain18: '20'
    if_gain19: '20'
    if_gain2: '20'
    if_gain20: '20'
    if_gain21: '20'
    if_gain22: '20'
    if_gain23: '20'
    if_gain24: '20'
    if_gain25: '20'
    if_gain26: '20'
    if_gain27: '20'
    if_gain28: '20'
    if_gain29: '20'
    if_gain3: '20'
    if_gain30: '20'
    if_gain31: '20'
    if_gain4: '20'
    if_gain5: '20'
    if_gain6: '20'
    if_gain7: '20'
    if_gain8: '20'
    if_gain9: '20'
    iq_balance_mode0: '0'
    iq_balance_mode1: '0'
    iq_balance_mode10: '0'
    iq_balance_mode11: '0'
    iq_balance_mode12: '0'
    iq_balance_mode13: '0'
    iq_balance_mode14: '0'
    iq_balance_mode15: '0'
    iq_balance_mode16: '0'
    iq_balance_mode17: '0'
    iq_balance_mode18: '0'
    iq_balance_mode19: '0'
    iq_balance_mode2: '0'
    iq_balance_mode20: '0'
    iq_balance_mode21: '0'
    iq_balance_mode22: '0'
    iq_balance_mode23: '0'
    iq_balance_mode24: '0'
    iq_balance_mode25: '0'
    iq_balance_mode26: '0'
    iq_balance_mode27: '0'
    iq_balance_mode28: '0'
    iq_balance_mode29: '0'
    iq_balance_mode3: '0'
    iq_balance_mode30: '0'
    iq_balance_mode31: '0'
    iq_balance_mode4: '0'
    iq_balance_mode5: '0'
    iq_balance_mode6: '0'
    iq_balance_mode7: '0'
    iq_balance_mode8: '0'
    iq_balance_mode9: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    num_mboards: '1'
    sample_rate: samp_rate
    sync: sync
    time_source0: ''
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [67, 325]
    rotation: 0
    state: enabled
- name: rfg
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: RF Gain
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [797, 15]
    rotation: 0
    state: true

connections:
- [analog_sig_source_x_0, '0', blocks_multiply_xx_0, '1']
- [blocks_complex_to_float_0, '0', audio_sink_0, '0']
- [blocks_multiply_const_vxx_0, '0', osmosdr_sink_0, '0']
- [blocks_multiply_const_vxx_0_0, '0', blocks_complex_to_float_0, '0']
- [blocks_multiply_xx_0, '0', blocks_multiply_const_vxx_0_0, '0']
- [blocks_repack_bits_bb_0, '0', blocks_unpacked_to_packed_xx_0, '0']
- [blocks_udp_source_0, '0', eight_psk_encoder_0, '0']
- [blocks_unpacked_to_packed_xx_0, '0', digital_constellation_modulator_0, '0']
- [blocks_vector_source_x_2_1_0, '0', eight_psk_encoder_0, '0']
- [digital_constellation_modulator_0, '0', interp_fir_filter_xxx_0_0, '0']
- [eight_psk_encoder_0, '0', blocks_repack_bits_bb_0, '0']
- [interp_fir_filter_xxx_0, '0', blocks_multiply_const_vxx_0, '0']
- [interp_fir_filter_xxx_0_0, '0', analog_sig_source_x_0, freq]
- [interp_fir_filter_xxx_0_0, '0', interp_fir_filter_xxx_0, '0']
- [osmosdr_source_0, '0', blocks_null_sink_0, '0']

metadata:
  file_format: 1
