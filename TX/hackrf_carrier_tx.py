#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Not titled yet
# GNU Radio version: 3.8.0.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from gnuradio import analog
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio.qtgui import Range, RangeWidget
import osmosdr
import time
from gnuradio import qtgui

class hackrf_carrier_tx(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Not titled yet")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Not titled yet")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "hackrf_carrier_tx")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000
        self.rfg = rfg = 61
        self.ifg = ifg = 0
        self.carrier = carrier = 1
        self.bbg = bbg = 0

        ##################################################
        # Blocks
        ##################################################
        self._rfg_range = Range(1, 100, 1, 61, 200)
        self._rfg_win = RangeWidget(self._rfg_range, self.set_rfg, 'rfg', "counter_slider", float)
        self.top_grid_layout.addWidget(self._rfg_win)
        self._ifg_range = Range(0, 100, 1, 0, 200)
        self._ifg_win = RangeWidget(self._ifg_range, self.set_ifg, 'ifg', "counter_slider", float)
        self.top_grid_layout.addWidget(self._ifg_win)
        self._carrier_range = Range(0, 100, .1, 1, 200)
        self._carrier_win = RangeWidget(self._carrier_range, self.set_carrier, 'carrier', "counter_slider", float)
        self.top_grid_layout.addWidget(self._carrier_win)
        self._bbg_range = Range(0, 100, 1, 0, 200)
        self._bbg_win = RangeWidget(self._bbg_range, self.set_bbg, 'bbg', "counter_slider", float)
        self.top_grid_layout.addWidget(self._bbg_win)
        self.osmosdr_sink_0 = osmosdr.sink(
            args="numchan=" + str(1) + " " + 'soapy=0,driver=hackrf'
        )
        self.osmosdr_sink_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_sink_0.set_sample_rate(1000000)
        self.osmosdr_sink_0.set_center_freq(2400100000, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(rfg, 0)
        self.osmosdr_sink_0.set_if_gain(ifg, 0)
        self.osmosdr_sink_0.set_bb_gain(bbg, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)
        self.analog_const_source_x_0 = analog.sig_source_c(0, analog.GR_CONST_WAVE, 0, 0, carrier)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0, 0), (self.osmosdr_sink_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "hackrf_carrier_tx")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_rfg(self):
        return self.rfg

    def set_rfg(self, rfg):
        self.rfg = rfg
        self.osmosdr_sink_0.set_gain(self.rfg, 0)

    def get_ifg(self):
        return self.ifg

    def set_ifg(self, ifg):
        self.ifg = ifg
        self.osmosdr_sink_0.set_if_gain(self.ifg, 0)

    def get_carrier(self):
        return self.carrier

    def set_carrier(self, carrier):
        self.carrier = carrier
        self.analog_const_source_x_0.set_offset(self.carrier)

    def get_bbg(self):
        return self.bbg

    def set_bbg(self, bbg):
        self.bbg = bbg
        self.osmosdr_sink_0.set_bb_gain(self.bbg, 0)



def main(top_block_cls=hackrf_carrier_tx, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
