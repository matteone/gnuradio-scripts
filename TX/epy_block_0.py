"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr

class blk(gr.basic_block):  # other base classes are basic_block, decim_block, interp_block

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='Morse Encoder',   # will show up in GRC
            in_sig=[np.byte],
            out_sig=[np.complex64]
        )
        self.eol = True
        # International morse code (sample)
        self.morse = {
            # Letters
            "a": ".-",
            "b": "-...",
            "c": "-.-.",
            "d": "-..",
            "e": ".",
            "f": "..-.",
            "g": "--.",
            "h": "....",
            "i": "..",
            "j": ".---",
            "k": "-.-",
            "l": ".-..",
            "m": "--",
            "n": "-.",
            "o": "---",
            "p": ".--.",
            "q": "--.-",
            "r": ".-.",
            "s": "...",
            "t": "-",
            "u": "..-",
            "v": "...-",
            "w": ".--",
            "x": "-..-",
            "y": "-.--",
            "z": "--..",
            # Numbers
            "0": "-----",
            "1": ".----",
            "2": "..---",
            "3": "...--",
            "4": "....-",
            "5": ".....",
            "6": "-....",
            "7": "--...",
            "8": "---..",
            "9": "----.",
            # Punctuation
            "&": ".-...",
            "'": ".----.",
            "@": ".--.-.",
            ")": "-.--.-",
            "(": "-.--.",
            ":": "---...",
            ",": "--..--",
            "=": "-...-",
            "!": "-.-.--",
            ".": ".-.-.-",
            "-": "-....-",
            "+": ".-.-.",
            '"': ".-..-.",
            "?": "..--..",
            "/": "-..-.",
        }

    def general_work(self, input_items, output_items):
        #print "requested output items:", len(output_items[0])
        #print "passed input items", len(input_items[0])
        if len(input_items[0]) == 0:
            #print "no input items"
            return 0
        output = self.translate_text([input_items[0][0]])
        #print "produced items:", len(output)
        if len(output) > len(output_items[0]):
            #print "too few output items requested"
            return 0
        output_items[0][:len(output)] = [ x for x in output ]
        self.consume_each(1)
        return len(output)

    def forecast(self, ninput_items_required, noutput_items):
        #print noutput_items
        ninput_items_required = int(noutput_items / 14)
        return ninput_items_required

    def translate_text(self,bytearray):
        translation = []
        for byte in bytearray:
            char = chr(byte)
            if char.lower() in self.morse:
                morsecode = self.morse[char.lower()]
                if self.eol:
                    self.eol = False
                    translation.extend([0,0,0])
                for dihdah in morsecode:
                    if dihdah == '.':
                        translation.extend([1,0])
                    else:
                        translation.extend([1,1,1,0])
                translation.extend([0,0])
            elif (char == ' '):
                morsecode = 'space'
                translation.extend([0,0,0,0])
            elif (char == '\n'):
                morsecode = 'eol'
                translation.extend([0,0,0,0,0,0,0,0])
                self.eol = True
            else:
                morsecode = 'n/a'
            print(char,morsecode,translation)
        return translation
        
